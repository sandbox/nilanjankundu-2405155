<?php

/**
 * @file
 * admin.inc - Slider Captcha module code menu callback functions for admin pages.
 */

/**
 * Callback for admin/slider_captcha
 * Defines the settings page with list and form.
 */
function slider_captcha_admin_forms() {
  $output = '';
  $rows = array();
  $header = array(t('Form ID'), t('Operation'));
  
  $query = db_select('slider_captcha_forms', 'sc')
           ->fields('sc', array('id', 'form_id'))
           ->execute();
  foreach ($query as $data) {
      $rows[] = array($data->form_id, l(t("Delete"), 'admin/config/development/slider-captcha/delete/' . $data->id));
  }

  if (count($rows)) {
    $output = theme('table', array(
                          'header' => $header,
                          'rows' => $rows
                        ));
  }
  $output .= drupal_render(drupal_get_form("slider_captcha_admin_forms_add"));

  return $output;
}

/**
 * Form builder;
 *
 * Defines the settings form.
 */
function slider_captcha_admin_forms_add() {
  $form['new_form_id'] = array(
    '#description' => t('This is the drupal form id of the form.'),
    '#required' => TRUE,
    '#title' => t('Form ID'),
    '#type' => 'textfield',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Form'),
  );
  $form['#validate'][] = 'slider_captcha_admin_forms_add_validate';
  $form['#submit'][] = 'slider_captcha_admin_forms_add_submit';
  return $form;
}

/**
 * Settings form validate.
 */
function slider_captcha_admin_forms_add_validate($form, $form_state) {
  if (db_query("SELECT COUNT(id) AS count FROM {slider_captcha_forms} WHERE form_id = :fid", array(':fid' => $form_state['values']['new_form_id']))->fetchField()) {
    form_set_error('new_form_id', t('The form id %form_id already exists. Please enter another form id.', array('%form_id' => $form_state['values']['new_form_id'])));
  }
}

/**
 * Settings form submit.
 */
function slider_captcha_admin_forms_add_submit($form, $form_state) {
  if (!empty($form_state['values']['new_form_id'])) {
    
    db_insert('slider_captcha_forms')
              ->fields(array(
                'form_id' => $form_state['values']['new_form_id'],
              ))
              ->execute();
    
    drupal_set_message(t('The form id has been added.'));
  }
}

/**
 * Form builder; confirm form for slider captcha form id deletion.
 *
 * @ingroup forms
 * @see slider_captcha_admin_forms_confirm_delete_submit()
 */
function slider_captcha_admin_forms_confirm_delete($form, &$form_state, $captcha_form_id) {
  $form['slider_captcha_form_id'] = array(
    '#type' => 'value',
    '#value' => $captcha_form_id,
  );
  return confirm_form($form,  
        t('Are you sure you want to delete the form id?'),  
        'admin/config/development/slider-captcha',  
        t('The slider captcha will be removed from the form.'),  
        t('Delete'), t('Cancel'));
}

/**
 * Submit function for the confirm form for slider captcha form id deletion.
 */
function slider_captcha_admin_forms_confirm_delete_submit($form, &$form_state) {
  if (isset($form_state['values']['slider_captcha_form_id'])) {
    $query = db_delete('slider_captcha_forms') 
           ->condition('id', $form_state['values']['slider_captcha_form_id'])
           ->execute();
    drupal_set_message(t('The form has been deleted.'));
  }

  if (!isset($_REQUEST['destination'])) {
    $form_state['redirect'] = 'admin/config/development/slider-captcha';
  }
}
