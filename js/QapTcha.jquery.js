/**
 * @file
 * QapTcha - jQuery Plugin
 * Open Source - MIT License : http://www.opensource.org/licenses/mit-license.php
 */

jQuery.QapTcha = {
	build : function(options)
	{
			var defaults = {
			txtLock :  Drupal.t("LOCKED: Drag to the right, then submit your form."),
			txtUnlock : Drupal.t("UNLOCKED: You can submit your form now."),
			disabledSubmit : true,
			autoRevert : true,
			PHPfile : Drupal.settings.ajaxfile,
			autoSubmit : false
        };

		if(this.length>0)
		return jQuery(this).each(function(i) {
			/** Vars **/
			var
				opts = jQuery.extend(defaults, options),
				$this = jQuery(this),
				form = jQuery('form').has($this),
				Clr = jQuery('<div>',{'class':'clr'}),
				bgSlider = jQuery('<div>',{'class':'bgSlider'}),
				Slider = jQuery('<div>',{'class':'Slider'}),
				TxtStatus = jQuery('<div>',{'class':' TxtStatus dropError'}).html(opts.txtLock),
				inputQapTcha = jQuery('<input>',{name:generatePass(32),value:generatePass(7),type:'hidden'});

			/** Disabled submit button **/
			if(opts.disabledSubmit) form.find('input[type=\'submit\']').attr('disabled','disabled');

			/** Construct DOM **/
			bgSlider.appendTo($this);
			Clr.insertBefore(bgSlider);
			TxtStatus.insertBefore(Clr);
			inputQapTcha.appendTo($this);
			Slider.appendTo(bgSlider);
			$this.show();

			Slider.draggable({
				revert: function(){
					if(opts.autoRevert)
					{
						if(parseInt(Slider.css("left")) > (bgSlider.width()-Slider.width()-10)) return false;
						else return true;
					}
				},
				containment: bgSlider,
				axis:'x',
				stop: function(event,ui){
					if(ui.position.left > (bgSlider.width()-Slider.width()-10))
					{
						// set the SESSION iQaptcha in PHP file
						jQuery.post(Drupal.settings.basePath + opts.PHPfile,{
							action : 'qaptcha',
							qaptcha_key : inputQapTcha.attr('name')
						},
						function(data) {
							if(!data.error)
							{
								$this.addClass('unlocked');
								Slider.draggable('disable').css('cursor','default');
								inputQapTcha.val('');
								TxtStatus.html(opts.txtUnlock).addClass('dropSuccess').removeClass('dropError');
								form.find('input[type=\'submit\']').removeAttr('disabled');
								if(opts.autoSubmit) form.find('input[type=\'submit\']').trigger('click');
							}
						},'json');
					}
				}
			});

			function generatePass(nb) {
		        var chars = 'azertyupqsdfghjkmwxcvbn23456789AZERTYUPQSDFGHJKMWXCVBN_-#@';
		        var pass = '';
		        for(i=0;i<nb;i++){
		            var wpos = Math.round(Math.random()*chars.length);
		            pass += chars.substring(wpos,wpos+1);
		        }
		        return pass;
		    }

		});
	}
}; jQuery.fn.QapTcha = jQuery.QapTcha.build;